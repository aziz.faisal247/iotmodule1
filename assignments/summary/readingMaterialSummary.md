# IoT Basics
## Industrial Revolution
![](https://gitlab.com/aziz.faisal247/iotmodule1/-/raw/master/extras/Industrial_Revolution.jpg)
***
## Industry 3.0 (Third Industrial Revolution)
It includes :
- **Field Devices:** These are the sensors and actuators that measure parameters and perfomr action.
- **Control Devices:** These control a bunch of field devices by logical operations.
- **Station:** A group of control devices working on the production of a particular component of a product.
- **Work Centre:** A factory having different stations.
- **Enterprise:** Owner of such factories.

## Communication Architecture of Industry 3.0 Devices
- Sensors measure the physical parameters such as temperature, fluid pressure, humidity, etc.
- The data is then transferred to the control devices through a communication protocol (language).
    - Common protocols are Modbus, EtherCAT, etc
- The control devices such as PLCs perform logical analysis and sends instructions to actuators which perform actions.
- Control Devices simultaneously send the data to a local database to store the data. 

## Industry 4.0 (Fourth Industrial Revolution)
- Industry 3.0 devices connected to the internet.
- Data is stored on remote servers in the cluds from where the data can be accessed from any place.
- Stastical Analysis can be done on the cloud and data can be displayed in an organised manner to aid analysis.
- Activate alerts in case of emergencies.

## Communication Architecture of Industry 4.0 Devices
- Sensors send data to control devices.
- Control devices send this data to the local server as well as the remote server using Industry 4.0 Communication Protocol.
    - Common protocols are MQTT, Websockets, etc.
- Data stored in the cloud is then analysed through various platforms through internet.

## Problems in conversion
- Industry 4.0 devices are expensive to be implemented on industrial scale.
- Industry cannot be shutdown for long for upgradation.
- Questions on reliability.
### Solution
- Get data from Industry 3.0 devices and send it to the remote server with the help of Industry 4.0 devices.
- This can be done by converting 3.0 protocols to 4.0 protocols with the help of IoT devices.
    - Raspberry Pi 3
    - Raspberry Pi 4
    - Rock Pi S 
    - Many more...
- There are still some challenges such as:
    - The hardware is expensive
    - Protocols are not properly documented
    - Only the manufacturer knows the details of the protocols
### Roadmap
1. Identify most popular Industry 3.0 devices.
2. Study protocols that these devices communicate.
3. Get data from Industry 3.0 Devices
4. Send data to cloud for Industry 4.0

## Web based Data Analysis
Once the data is available on the internet, it can be analysed with the help of various Tools available online
- **IoT Time Series Database (TSDB) Tools:** Stores the data in time series databases.
    - *Prometheus*
    - *InfluxDB*
- **IoT Dashboards:** View all the data in interactive dashboards.
    - *Grafana*
    - *Thingsboard*
- **IoT platforms:** Analyse the data on these platforms.
    - *AWS IoT*
    - *Google IoT*
    - *Azure IoT*
    - *Thingsboard*
- **Get Alerts:** Get alerts based on the data available.
    - *Twilio*
    - *Zaiper*